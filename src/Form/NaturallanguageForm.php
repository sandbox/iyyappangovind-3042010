<?php

namespace Drupal\natural_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Natural language form class created.
 */
class NaturallanguageForm extends FormBase {

  /**
   * Natural language form class created.
   */
  public function getFormId() {
    return 'naturallanguageForm';
  }

  /**
   * Natural language form class created.
   */
  public function getTaxanomytree() {
    $path = drupal_get_path('module', 'general');
    // Render the main scripts file.
    // List of selected term.
    $config = $this->config('natural_form.adminsettings');
    $vocabulary_tid = $config->get('Vocabulary_list');
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
      ->loadTree($vocabulary_tid);
     // print_r($terms);
    foreach ($terms as $item) {
      if ($item->depth == 0) {
        $parent = $item->tid;
        $parent_ids[$parent][$item->tid] = $item->name;
        $parents[$item->tid] = $item->name;
      }
      elseif ($item->depth == 1) {
        $child = $item->tid;
        $parent_ids[$parent]['child'][$item->tid] = $item->name;
      }
      elseif ($item->depth == 2) {
        $term_object = taxonomy_term_load($item->tid);
      
        $term_url = $term_object->get('field_url_faq')->value;
        //kint($term_url);
        //$term_url = $term_url ? Url::fromUri($term_url)->toString() : '';
        $parent_ids[$parent][$child]['subchild'][$item->tid] = ['name' => $item->name, 'url' => $term_url];
      }
    }
    return ['tree' => $parent_ids, 'parent' => $parents];
  }

  /**
   * Natural language form class created.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('natural_form.adminsettings');
    $level1_name = $config->get('level1_name');
    $level2_name = $config->get('level2_name');
    $level3_name = $config->get('level3_name');
    $form['#attached']['library'][] = 'general/dependent_forms';
    $parent_ids = $this->getTaxanomytree();
    $parents = $parent_ids['parent'];
    $parent_tree = $parent_ids['tree'];
    $data = & drupal_static(__FUNCTION__);
    $cid = 'naturalform';
    if ($cache = \Drupal::cache()->get($cid)) {
      $data = $cache->data;
    }
    else {
      $data = $parent_ids;
      \Drupal::cache()->set($cid, $data);
    }

    $form['markup_fieldtitle'] = [
      '#type' => 'markup',
      '#title' => ('markup_contenttitle') ,
      '#markup' => 'HOW DO I APPLY?',
    ];

    $form['markup_field'] = [
      '#type' => 'markup',
      '#title' => ('markup_content') ,
      '#markup' => ' Please select the option bellow so we can help.',
    ];

    $form['state'] = [
      '#title' => $level1_name,
      '#type' => 'select',
      '#options' => $parents,
    ];

    $form['constituency_name'] = [
      '#title' => $level2_name ,
      '#type' => 'select',
      '#options' => [],
    ];
    $form['candidate-name'] = [
      '#title' => $level3_name,
      '#type' => 'select',
      '#options' => [],
    ];
    //print_r($parent_tree); die;
    $form['submit'] = ['#type' => 'button', '#value' => $this->t('Submit')];
    $form['#attached']['drupalSettings']['generalparent_tree'] = $parent_tree;
    return $form;
  }

  /**
   * Natural language submitform.
   */
  public function submitForm(array & $form, FormStateInterface $form_state) {
  }

}
