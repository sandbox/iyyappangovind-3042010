<?php

namespace Drupal\natural_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Natural form class created.
 */
class NaturalForm extends ConfigFormBase {

  /**
   * Create form name.
   */
  protected function getEditableConfigNames() {
    return [
      'natural_form.adminsettings',
    ];
  }

  /**
   * Create unique form id.
   */
  public function getFormId() {
    return 'natural_form_form';
  }

  /**
   * Build Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $vocabulary_list = db_query("select vid, vid from {taxonomy_term_data} ")->fetchAllkeyed();
    $array1 = ['_none' => '-None-'];
    $array2 = $vocabulary_list;
    $allvocabulary_list = array_merge($array1, $array2);
    $config = $this->config('natural_form.adminsettings');

    $form['natural_form'] = [
      '#type' => 'fieldset',
      '#title' => t('Natural Form Page Settings'),
      '#collapsible' => TRUE, 
      '#collapsed' => FALSE, 
    ];

    $form['natural_form']['vocabularys'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose Vocabulary'),
      '#description' => $this->t('Selelct Natural Form Vocabulary List'),
      '#options' => $allvocabulary_list,
      '#default_value' => $config->get('Vocabulary_list'),
    ];
    $form['natural_form']['level1_name'] = [
      '#type' => 'textfield',
      '#title' => t('Level1 Name:'),
      '#required' => TRUE,
      '#default_value' => $config->get('level1_name'),
    ];
    $form['natural_form']['level2_name'] = [
      '#type' => 'textfield',
      '#title' => t('Level2 Name:'),
      '#required' => TRUE,
      '#default_value' => $config->get('level2_name'),
    ];
    $form['natural_form']['level3_name'] = [
      '#type' => 'textfield',
      '#title' => t('Level3 Name:'),
      '#required' => TRUE,
      '#default_value' => $config->get('level3_name'),
    ];

    $form['natural_form']['markup_thirdlevel_note'] = [
      '#type' => 'markup',
      '#title' => ('markup_contenttitle') ,
      '#markup' => 'Make sure the third level term should have a URL link field',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('natural_form.adminsettings')
      ->set('Vocabulary_list', $form_state->getValue('vocabularys'))
      ->set('level1_name', $form_state->getValue('level1_name'))
      ->set('level2_name', $form_state->getValue('level2_name'))
      ->set('level3_name', $form_state->getValue('level3_name'))->save();
  }

}
