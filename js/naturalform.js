(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.naturalBehavior = {
    attach: function (context, settings) {
      // can access setting from 'drupalSettings';
      var term_data_arr = drupalSettings.generalparent_tree;

      $(document).on('change', '#edit-state',function(){
          $('#edit-constituency-name').empty();
          $('#edit-candidate-name').empty();
          var statekey = $(this).find('option:selected').val();
          for(var key in term_data_arr[statekey]['child']){
            $('#edit-constituency-name').append(
            $('<option></option>').val(key).html(term_data_arr[statekey]['child'][key])
            );
          }
          var citykey = $('#edit-constituency-name').val();
          for(var key in term_data_arr[statekey][citykey]['subchild']){
            $('#edit-candidate-name').append(
            $('<option></option>').val(key).html(term_data_arr[statekey][citykey]['subchild'][key]['name'])
            );
          }

      });

      $(document).on('change', '#edit-constituency-name',function(){
    
        $('#edit-candidate-name').empty();
        var citykey = $(this).find('option:selected').val();
        var statekey = $('#edit-state').val();
        //console.log(term_data_arr[statekey][citykey]['subchild']);
        for(var key in term_data_arr[statekey][citykey]['subchild']){
          $('#edit-candidate-name').append(
          $('<option></option>').val(key).html(term_data_arr[statekey][citykey]['subchild'][key]['name'])
          );
        }
        
      });

      /*$( "#edit-submit" ).submit(function( event ) {
        console.log('test');
        alert( "Handler for .submit() called." );
        event.preventDefault();
      });*/

      $( "form" ).submit(function( event ) {
        var statekey = $('#edit-state').val();
        var citykey = $('#edit-constituency-name').val();
        var candidatekey = $('#edit-candidate-name').val();
        //console.log(statekey +' '+ citykey +' '+ candidatekey);
        var faqUrl = term_data_arr[statekey][citykey]['subchild'][candidatekey]['url'];
        //console.log(statekey +' '+ citykey +' '+ candidatekey +'---'+ faqUrl);        
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+ "/" +getUrl.pathname.split('/')[2];
        console.log(baseUrl);
        event.preventDefault();

        location.href= baseUrl+faqUrl;
      });

    }
  };
})(jQuery, Drupal, drupalSettings );
