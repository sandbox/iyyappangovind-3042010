<?php

namespace Drupal\natural_form\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "Natural_language_block",
 *   admin_label = @Translation("Natural Language Block"),
 * )
 */
class Naturallanguageblock extends BlockBase {

  /**
   * Append form inside the block.
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\natural_form\Form\NaturallanguageForm');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

}
